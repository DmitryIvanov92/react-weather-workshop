import axios from 'axios';

const config = {
  owmKey: '5286dad53c093f2cd6f587a908f0e752', // openweathermap key
};

export const getUserLocationByIP = () => axios.get('http://freegeoip.net/json/');

export const getWeatherByPlace = name => axios.get(`http://api.openweathermap.org/data/2.5/weather?q=${name}&APPID=${config.owmKey}`);
