import React, { Component, PropTypes } from 'react';
import { Button } from 'react-bootstrap';

class TabTitle extends Component {
  constructor(props) {
    super(props);
    this.onDelete = this.onDelete.bind(this);
  }

  onDelete() {
    this.props.onDelete(this.props.elName);
  }

  render() {
    return (
      <div>
        <div className="title">{this.props.elName}</div>
        <Button bsSize="xsmall" onClick={this.onDelete}><span>X</span></Button>
      </div>
    );
  }
}

TabTitle.propTypes = {
  onDelete: PropTypes.func.isRequired,
  elName: PropTypes.string.isRequired,
};

export default TabTitle;
