# React Weather Workshop

- - -
![Screenshot](weatherApp-screen.png)

- - -
#### Used components
* [ReactJS]
  * [react-bootstrap]
  * [react-notify-toast]
* [Bootstrap]
* [axios]
* [moment]
* [localStorage]

- - -

#### Installing dependencies

```sh
$ npm install
```
or if you use Yarn
```sh
$ yarn install
```
- - -
#### Application starting in development mode

```sh
$ npm start
```
or if you use Yarn
```sh
$ yarn start
```
- - -
#### Building an application for production

```sh
$ npm run build
```
or if you use Yarn
```sh
$ yarn run start
```
- - -

[screenshot]: ./weatherApp-screen.png
[ReactJS]: <https://facebook.github.io/react/>
[react-bootstrap]: <https://react-bootstrap.github.io/>
[react-notify-toast]: <https://www.npmjs.com/package/react-notify-toast>
[Bootstrap]: <http://getbootstrap.com/>
[axios]: <https://github.com/mzabriskie/axios>
[moment]: <https://momentjs.com/>
[localStorage]: <https://github.com/coolaj86/node-localStorage>
